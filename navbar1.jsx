import React, { Component } from "react";

import { Link } from "react-router-dom";
import * as ReactBootStrap from "react-bootstrap";
class Navbar extends Component {
  state = {};
  render() {
    return (
      <div className="App">
        <ReactBootStrap.Navbar
          collapseOnSelect
          expand="lg"
          bg="dark"
          variant="dark"
        >
          <ReactBootStrap.Navbar.Brand href="#home">
            BookSite
          </ReactBootStrap.Navbar.Brand>
          <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
            <ReactBootStrap.Nav className="mr-auto">
              <ReactBootStrap.Nav.Link href="/books?newarrival=yes">
                New Arrival
              </ReactBootStrap.Nav.Link>
              <ReactBootStrap.NavDropdown
                title="Books By Genre"
                id="collasible-nav-dropdown"
              >
                <ReactBootStrap.NavDropdown.Item href="/books/fiction">
                  Fiction
                </ReactBootStrap.NavDropdown.Item>
                <ReactBootStrap.NavDropdown.Item href="/books/children">
                  Children
                </ReactBootStrap.NavDropdown.Item>
                <ReactBootStrap.NavDropdown.Item href="/books/mystery">
                  Mystery
                </ReactBootStrap.NavDropdown.Item>
                <ReactBootStrap.NavDropdown.Item href="/books/management">
                  Management
                </ReactBootStrap.NavDropdown.Item>
                <ReactBootStrap.NavDropdown.Item href="/books/selfhelp">
                  Self Help
                </ReactBootStrap.NavDropdown.Item>
              </ReactBootStrap.NavDropdown>
              <ReactBootStrap.Nav.Link href="/books">
                All Books
              </ReactBootStrap.Nav.Link>
            </ReactBootStrap.Nav>
          </ReactBootStrap.Navbar.Collapse>
        </ReactBootStrap.Navbar>
      </div>
    );
  }
}

export default Navbar;
