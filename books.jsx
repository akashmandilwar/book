import React, { Component } from "react";
import axios from "axios";
import queryString from "query-string";

const apiEndpoint = "http://localhost:2410/booksapp/books";
class books extends Component {
  state = {
    posts: [],
  };
  async componentDidUpdate(prevProps) {
	  if(prevProps!==this.props){
    const any = this.props.match.params.any;
    this.apiEndpoint =
      (any == undefined
        ? this.apiEndpoint + "/" + ""
        : this.apiEndpoint + "/" + any) + this.props.location.search;
    console.log(any);
    const { data: posts } = await axios.get(apiEndpoint);
    this.setState({ posts: posts.data });
    }
  }
  }
  async componentDidMount() {
    const any = this.props.match.params.any;
    this.apiEndpoint =
      (any == undefined
        ? this.apiEndpoint + "/" + ""
        : this.apiEndpoint + "/" + any) + this.props.location.search;
    console.log(any);
    const { data: posts } = await axios.get(apiEndpoint);

    console.log(posts.data);
    this.setState({ posts: posts.data });
  }
  callurl = (params, newArrival) => {
    let path = "/books";
    let newarrival = this.props.location.search;
    const any = this.props.match.param;
    if (any) path = path + "/" + any;
    params = this.addToParams(params, newArrival, newarrival);
    this.props.history.push({
      pathname: path,
      search: params,
    });
  };
  addToParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  gotopage = (x) => {
    let { posts, page } = queryString.parse(this.props.location.search);
    let currPage = page ? +page : 1;
    currPage = currPage + x;
    let params = "";
    params = this.addToParams(params, "Page", currPage);
    this.callurl(params, "newArrival");
  };
  render() {
    console.log(this.props.match.params.any);
    return (
      <div>
        <table className="table">
          <thead>
            <tr border>
              <th
                style={{
                  background: "blue",
                  color: "black",
                }}
              >
                Title
              </th>
              <th
                style={{
                  background: "blue",
                  color: "black",
                }}
              >
                Author
              </th>
              <th style={{ background: "blue", color: "black" }}>Language</th>
              <th style={{ background: "blue", color: "black" }}>Genre</th>
              <th style={{ background: "blue", color: "black" }}>Price</th>
              <th style={{ background: "blue", color: "black" }}>
                Best Seller
              </th>
            </tr>
          </thead>
          <tbody>
            {this.state.posts.map((post) => (
              <tr key={post.name}>
                <td onClick={() => this.handleClick(post.name)}>{post.name}</td>
                <td>{post.author}</td>
                <td>{post.language}</td>
                <td>{post.genre}</td>
                <td>{post.price}</td>
                <td>{post.bestseller}</td>
              </tr>
            ))}
          </tbody>
        </table>{" "}
        <div className="row">
          <button
            className="btn btn-primary m-2 text-left"
            onClick={() => this.gotopage(-1)}
          >
            Previous
          </button>
          <button className="btn btn-primary" onClick={() => this.gotopage(1)}>
            Next
          </button>
        </div>
      </div>
    );
  }
}

export default books;
